include(CMakeForceCompiler)

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR cortex-m3)

find_program(ARM_CC      arm-none-eabi-gcc     /usr/bin ${TOOLCHAIN_DIR}/bin)
find_program(ARM_CXX     arm-none-eabi-g++     /usr/bin ${TOOLCHAIN_DIR}/bin)
find_program(ARM_OBJCOPY arm-none-eabi-objcopy /usr/bin ${TOOLCHAIN_DIR}/bin)
find_program(ARM_SIZE    arm-none-eabi-size    /usr/bin ${TOOLCHAIN_DIR}/bin)
find_program(ARM_AS      arm-none-eabi-as      /usr/bin ${TOOLCHAIN_DIR}/bin)

cmake_force_c_compiler(${ARM_CC} GNU)
cmake_force_cxx_compiler(${ARM_CXX} GNU)

#set(CMAKE_C_COMPILER ${ARM_CC})
#set(CMAKE_CXX_COMPILER ${ARM_CXX})

set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -mcpu=${CMAKE_SYSTEM_PROCESSOR} -mthumb")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

#include "mainloop.h"

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>

#include <stdio.h>
#include <string.h>

static bool esp_connected = false;
static char esp_buf[256];
static uint8_t esp_count;
static char esp_line[256];

static char p_buf[256];
static uint8_t p_count;
static char p_line[256];

void mainloop()
{
	// if sd is not yet initialized but sd card was inserted do so
	if (!sd.isOpen() && gpio_get(GPIOA, GPIO1)==0)
	{
		printf("Initializing SD Card\r\n");
		if (sd.open())
			printf("SD Card initialized\r\n");
		else
			printf("SD Card failed\r\n");
	}
	// if the sd card is not present, wait and retry
	if (!sd.isOpen())
	{
		for (uint16_t i = 0; i < 1e3; i++)
			__asm__("nop");
		return;
	}
	
	// if the connection to the ESP was not established, send a hello
	if (!esp_connected)
	{
		// check line
		if (*esp_line && strcmp(esp_line, "HELLO") == 0)
		{
			esp_connected = true;
			gpio_clear(GPIOC, GPIO13);
			printf("Connected to the ESP\r\n");
			memset(esp_line, 0, 256);
			return;
		}
		
		printf("Not connected to the ESP\r\n");
		dprintf(ESP_FILENO, "HELLO\r\n");
		// give the esp some time to answer
		for (uint16_t i = 0; i < 5e3; i++)
			__asm__("nop");
		// no other actions until esp connected
		return;
	}
	
	// if there is a chunk of data available from the esp, process it
	if (*esp_line)
	{
		esp_recv(esp_line);
		memset(esp_line, 0, 256);
	}
	
	// if there is a chunk of data available from the printer, process it
	if (*p_line)
	{
		p_recv(p_line);
		memset(p_line, 0, 256);
	}
	
	
	// if we are currently printing, see whether we should proceed
	if (mode == Printing)
	{
		mode = PrintingIdle; // will be re-enabled on recv of 'ok'
		
		uint16_t i = 0;
		for (; i < FILE_BUF_PRINTING_OFF; i++)
			f_buf[i] = ' ';
		int8_t c;
		for (; ((c = f.read()) != '\n') && (c != EOF); i++)
			f_buf[i] = c;
		f_buf[i] = 0;
		p_printing();
		if (c == EOF)
			mode = Idle;
	}
	
	
	
	
	__asm__("nop");
	return;
}

extern "C"
void usart2_isr()
{
	static uint8_t data = 'A';
	
	if (((USART_CR1(USART2) & USART_CR1_RXNEIE) != 0) &&
		((USART_SR(USART2) & USART_SR_RXNE) != 0))
	{
		data = usart_recv(USART2);
		//usart_send_blocking(USART1, data);
		if (data=='\r' || data=='\n')
		{
			if (esp_count != 0)
			{
				memcpy(esp_line, esp_buf, 256);
				esp_count = 0;
				memset(esp_buf, 0, 256);
			}
		}
		else if (data <= 0x7F)
		{
			esp_buf[esp_count] = data;
			esp_count++;
		}
	}
}

extern "C"
void usart3_isr()
{
	static uint8_t data = 'A';
	
	if (((USART_CR1(USART3) & USART_CR1_RXNEIE) != 0) &&
		((USART_SR(USART3) & USART_SR_RXNE) != 0))
	{
		data = usart_recv(USART3);
		//usart_send_blocking(USART1, data);
		if (data=='\r' || data=='\n')
		{
			if (p_count != 0)
			{
				memcpy(p_line, p_buf, 256);
				p_count = 0;
				memset(p_buf, 0, 256);
			}
		}
		else if (data <= 0x7F)
		{
			p_buf[p_count] = data;
			p_count++;
		}
	}
}

#include "mainloop.h"
#include "util.h"

#include <libopencm3/stm32/usart.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

void esp_recv(char *line)
{
	if (line[0] == CMD_SEND_TO_PRINTER) // send cmd to printer
	{
		dprintf(PRINTER_FILENO, "\r\n%s\r\n", line+1);
	}
	
	else if (line[0] == CMD_LIST_FILES) // list files
	{
		dprintf(ESP_FILENO, "F%d\r\n", sd.part.root().size);
	}
	
	else if (line[0] == CMD_GET_FILENAME) // get filename and size
	{
		long i = atol(line+1);
		dprintf(ESP_FILENO, "N%" PRIu32 ":%s\r\n", sd.part.root().files[i].size(), sd.part.root().files[i].name());
	}
	
	else if (line[0] == CMD_SELECT_FILE) // select file
	{
		if (lineno != -1)
			return;
		selected = atol(line+1);
		f = sd.part.root().files[selected];
	}
	
	else if (line[0] == CMD_READ_BYTES) // read the next bytes
	{
		if (lineno != -1)
			return;
		for (uint16_t i = 0; i < FILE_BUF_SIZE; i++)
			f_buf[i] = f.read();
		f_buf[FILE_BUF_SIZE] = 0;
		f_buf[FILE_BUF_SIZE] = checksum((char*)f_buf);
		usart_send_blocking(USART2, 'R');
		printBase64(USART2, (char*)f_buf, FILE_BUF_SIZE+1);
		dprintf(ESP_FILENO, "\r\n");
	}
	
	else if (line[0] == CMD_ECHO_BYTES) // echo the last bytes
	{
		if (lineno != -1)
			return;
		usart_send_blocking(USART2, 'R');
		printBase64(USART2, (char*)f_buf, FILE_BUF_SIZE+1);
		dprintf(ESP_FILENO, "\r\n");
	}
	
	else if (line[0] == CMD_START_PRINTING)
	{
		if (lineno != -1)
			return;
		lineno = 0;
		
		// re-open the file in case the pointer isn't at the beginning
		f = sd.part.root().files[selected];
		
		// tell the mainloop to print
		mode = Printing;
	}
	
	else if (line[0] == CMD_INTERRUPT_PRINTING)
	{
		mode = PrintingInterrupted;
	}
	
	else if (line[0] == CMD_CONTINUE_PRINTING)
	{
		mode = Printing;
	}
	
	else if (line[0] == CMD_STOP_PRINTING)
	{
		// release the printing mode so sd card can be accessed otherwise again
		mode = Idle;
		lineno = -1;
	}
	
	else
		printf("ESP: Unhandled line received: %s\r\n", line);
}

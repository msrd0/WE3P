#pragma once

#include "common.h"

#include <stm32sd/sd.h>

#define ESP_FILENO 2
#define PRINTER_FILENO 3

#define FILE_BUF_SIZE 1024
#define FILE_BUF_PRINTING_OFF 12

enum Mode : char { Idle, Printing, PrintingIdle, PrintingInterrupted };

extern SdSpiIface sd;
extern long selected;
extern fat::File f;
extern int8_t f_buf[FILE_BUF_SIZE+1];
extern int32_t lineno;
extern Mode mode;

// mainloop.cpp
void mainloop();

// esp.cpp
void esp_recv(char *buf);

// printer.cpp
void p_recv(char *buf);
void p_printing();

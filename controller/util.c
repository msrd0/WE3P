#include "util.h"

char checksum(char* buf)
{
	char cs = 0;
	for (char *p = buf; *p; p++)
		cs ^= *p;
	return cs;
}

static const char alphabet[64] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
	'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
	'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
	'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
	'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', '+', '/'
};

static uint32_t toBase64(char *src, uint8_t len)
{
	int pad = 3-len;
	uint32_t s = *src << 16;
	if (len > 1)
		s |= *(src+1) << 8;
	if (len > 2)
		s |= *(src+2);
	
	uint32_t r = 0;
	r |= alphabet[(s >> 18) & 0x3F] << 24;
	r |= alphabet[(s >> 12) & 0x3F] << 16;
	if (pad < 2)
		r |= alphabet[(s >> 6) & 0x3F] << 8;
	else
		r |= '=' << 8;
	if (pad < 1)
		r |= alphabet[s & 0x3F];
	else
		r |= '=';
	return r;
}

void printBase64(uint32_t usart, char *src, int32_t len)
{
	char *p = src;
	while (true)
	{
		uint32_t r = toBase64(p, len>=3 ? 3 : (len>=2 ? 2 : 1));
		usart_send_blocking(usart, (r>>24)&0xFF);
		usart_send_blocking(usart, (r>>16)&0xFF);
		usart_send_blocking(usart, (r>> 8)&0xFF);
		usart_send_blocking(usart, (r    )&0xFF);
		len -= 3;
		if (len <= 0)
			break;
		p += 3;
	}
}

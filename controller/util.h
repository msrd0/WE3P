#pragma once

#include <libopencm3/stm32/usart.h>

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

char checksum(char *buf);

void printBase64(uint32_t usart, char *src, int32_t len);

#ifdef __cplusplus
}
#endif

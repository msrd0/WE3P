#include <libopencm3/stm32/usart.h>

#include <errno.h>

// this shouldn't be neccessary - but anyway ...
void* __dso_handle;

int _write(int file, char *ptr, int len)
{
	int i;
	
	if (file == 1) {
		for (i = 0; i < len; i++)
			usart_send_blocking(USART1, ptr[i]);
		return i;
	}
	else if (file == 2) {
		for (i = 0; i < len; i++)
			usart_send_blocking(USART2, ptr[i]);
		return i;
	}
	else if (file == 3) {
		for (i = 0; i < len; i++)
			usart_send_blocking(USART3, ptr[i]);
		return i;
	}
	
	errno = EIO;
	return -1;
}

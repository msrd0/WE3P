#include "mainloop.h"
#include "util.h"

#include <inttypes.h>
#include <stdio.h>

void p_recv(char *line)
{
	dprintf(ESP_FILENO, "O%s\r\n", line);
	
	if (mode == PrintingIdle && line[0] == 'o' && line[1] == 'k')
		mode = Printing;
}

void p_printing()
{
	sprintf((char*)f_buf, "N%" PRId32, lineno);
	uint32_t len;
	// erase the damn null byte from sprintf
	for (len = 0; f_buf[len] != 0; len++);
	f_buf[len] = ' ';
	// get the length of the string
	for (len = 0; (f_buf[len] != 0) && (f_buf[len] != '\n') && (f_buf[len] != ';'); len++);
	f_buf[len] = 0;
	// get and print the checksum
	uint8_t cs = (uint8_t) checksum((char*)f_buf);
	sprintf((char*)(f_buf+len), "*%" PRIu8, cs);
	
	dprintf(ESP_FILENO, "P%s\r\n", f_buf);
	dprintf(PRINTER_FILENO, "\r\n%s\r\n", f_buf);
	
	lineno++;
}

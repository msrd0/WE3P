#include "server.h"

static bool connected = false;

inline void write(string str)
{
  Serial.write(str.data());
}

static string jsonbuf;
static int numfiles = 0;
static int currfile = 0;
void handlewebsocket(uint8_t num, WStype_t type, uint8_t *payload, size_t len)
{
  switch (type) {
    case WStype_CONNECTED:
      digitalWrite(ledw, 1);
      break;
    case WStype_DISCONNECTED:
      digitalWrite(ledw, 0);
      break;
    case WStype_TEXT: {
        string str = (char*)payload;
        if (str.substr(0, 5) == "hello")
          websocket.sendTXT(num, payload);
        else
          write(str + "\r\n");
      }
      break;
  }
}

static string buf;
void loop()
{
  while (Serial.available() > 0)
  {
    char c = Serial.read();
    if (c=='\r') c = '\n';
    buf.append(&c, 1);
    if (c=='\n')
      break;
  }

  if (buf.back() == '\n')
  {
    if (buf == "HELLO\n")
    {
      connected = true;
      Serial.write("HELLO\r\n");
      digitalWrite(ledh, 0);
    }

    else if (buf[0] == CMD_PRINTER_OUTPUT || buf[0] == CMD_READ_BYTES)
    {
      websocket.broadcastTXT(buf.data());
    }

    else if (buf[0] == CMD_LIST_FILES)
    {
      numfiles = atoi(buf.substr(1).data());
      currfile = 0;
      if (numfiles > 0)
      {
        jsonbuf = "F{";
        Serial.printf("%c0\r\n", CMD_GET_FILENAME);
      }
    }

    else if (buf[0] == CMD_GET_FILENAME)
    {
      size_t in = buf.find(':');
      jsonbuf += "\"" + buf.substr(in+1) + "\":{\"index\":" + to_string(currfile) + ",\"size\":" + buf.substr(1, in-1) + "},";
      currfile++;
      if (numfiles <= currfile)
      {
        jsonbuf += "\"null\":null}";
        websocket.broadcastTXT(jsonbuf.data());
        jsonbuf.clear();
        currfile = 0;
      }
      else
      {
        Serial.printf("%c%d\r\n", CMD_GET_FILENAME, currfile);
      }
    }

    buf.clear();
  }

  if (!connected)
    return;
  
  websocket.loop();
  server.handleClient();
}


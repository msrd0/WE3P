#define _GLIBCXX_USE_C99 1
#include <functional>
#include <string>

using std::string;
using std::to_string;

#include <Arduino.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>

#include "common.h"

#define ledh 4
#define ledw 5

// server.ino
extern ESP8266WebServer server;
extern WebSocketsServer websocket;

// http.cpp
void serve(String contenttype, String file);
void handleNotFound();

// main.cpp
void handlewebsocket(uint8_t num, WStype_t type, uint8_t *payload, size_t len);


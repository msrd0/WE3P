#include "server.h"

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <SPI.h>
#include <WiFiUdp.h>
#include <WebSocketsServer.h>

#include "config.h"

ESP8266WebServer server(80);
WebSocketsServer websocket(8000);

static ESP8266WiFiMulti wifiMulti;
void setup()
{
  Serial.begin(2e6);
  
  pinMode(ledh, OUTPUT);
  pinMode(ledw, OUTPUT);
  digitalWrite(ledh, 0);
  digitalWrite(ledw, 0);
  WiFi.mode(WIFI_STA);
  WiFi.hostname("WE3P");
  for (int i = 0; i < numssids; i++)
    wifiMulti.addAP(ssid[i], password[i]);

  // Wait for connection
  for (int i = 0; wifiMulti.run() != WL_CONNECTED; i++) {
    if (i > 10)
      ESP.restart();
    delay(500);
  }

  websocket.begin();
  websocket.onEvent(handlewebsocket);

  MDNS.begin("WE3P");
  MDNS.addService("http", "tcp", 80);
  MDNS.addService("ws", "tcp", 8000);

  SPIFFS.begin();

  server.on("/", HTTP_GET, [](){
    serve("text/html;charset=UTF-8", "/index.html");
  });
  server.on("/main.css", HTTP_GET, [](){
    serve("text/css;charset=UTF-8", "/main.css");
  });
  server.on("/main.js", HTTP_GET, [](){
    serve("text/javascript;charset=UTF-8", "/main.js");
  });
  server.on("/favicon.ico", HTTP_GET, [](){
    serve("image/x-icon", "/favicon.ico.gz");
  });

  server.onNotFound(handleNotFound);

  server.begin();

  digitalWrite(ledh, 1);
}


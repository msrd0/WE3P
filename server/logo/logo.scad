$fn = 200;

module text3d(s) { color([14*16/255, 7*16/255, 2*16/255, 1])
	linear_extrude(0.1) {
		text(s, 4);
	}
}

module nozzle() {
	// nozzle
	color("sandybrown")
		cylinder(r1=0.4, r2=4, h=7);
	color("darkgray") translate([0, 0, 7])
		cylinder(r=4, h=15);
	// heatblock
	color("dimgray") translate([-6, -6, 7])
		cube([16, 12, 5]);
	
	// text
	translate([-3, -6, 7.5])
		rotate([90, 0, 0])
			text3d("3P");
}

module chip() {
	module chipleg() {
		color("silver") translate([0, -0.15, 0]) {
			
			translate([0, 0, -0.15])
				cube([0.3, 0.3, 0.3]);
			translate([0.3, 0, -0.75])
				cube([0.3, 0.3, 0.9]);
			translate([0.6, 0, -0.75])
				cube([1, 0.3, 0.3]);
			
		}
	}
	
	side=10;
	color("black") translate([-side+1, -side+1, -1.5])
		cube([side, side, 1.5]);
	
	legsPerSide=12;
	
	// left
	for (i = [1:legsPerSide])
		translate([1, -side+1+side/(legsPerSide+1)*i, -0.75])
			chipleg();
	
	// front
	for (i = [1:legsPerSide])
		translate([-side+1+side/(legsPerSide+1)*i, 1, -0.75])
			rotate([0, 0, 90])
				chipleg();
	
	// right
	for (i = [1:legsPerSide])
		translate([-side+1, -side+1+side/(legsPerSide+1)*i, -0.75])
			rotate([0, 0, 180])
				chipleg();
	
	// front
	for (i = [1:legsPerSide])
		translate([-side+1+side/(legsPerSide+1)*i, -side+1, -0.75])
			rotate([0, 0, 270])
				chipleg();
	
	// text
	translate([-side/2-3, -side/2-2, 0])
		text3d("WE");
}

nozzle();
chip();

#!/bin/bash
set -e
cd "$(dirname "$0")"

file=$(mktemp -t 'we3p-logo-XXXXXX.png')

openscad \
	--camera=-0.3,-2.3,10.25,71.8,0,18.7,76.82 \
	--imgsize=2048,2048 \
	-o $file \
	--preview logo.scad

convert \
	$file \
	-fuzz 6% \
	-transparent white \
	logo.png

convert \
	logo.png \
	-resize 64x64 \
	../data/favicon.ico
gzip -f ../data/favicon.ico

rm $file

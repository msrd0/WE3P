To use this project, create a file called `config.h` containing the following:

```c++
const int numssids = 2;
const char* ssid[numssids] = {"SSID of your Router", "SSID2"};
const char* password[numssids] = {"Password of your Router", "Password2"};
```

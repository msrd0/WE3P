#pragma once

#define USART_SPEED 2e6

#define CMD_CONTINUE_PRINTING	'C' /*Continue*/
#define CMD_ECHO_BYTES			'E' /*Echo*/
#define CMD_LIST_FILES			'F' /*Files*/
#define CMD_START_PRINTING		'G' /*Go*/
#define CMD_INTERRUPT_PRINTING	'I' /*Interrupt*/
#define CMD_GET_FILENAME		'N' /*Name*/
#define CMD_PRINTER_OUTPUT		'O' /*Output*/
#define CMD_SEND_TO_PRINTER		'P' /*Printer*/
#define CMD_READ_BYTES			'R' /*Read*/
#define CMD_SELECT_FILE			'S' /*Select*/
#define CMD_STOP_PRINTING		'T' /*Terminate*/

var tabs = ['conn','cont','file','cons','sett','logi'];
var currtab = 'none';

var ws;
var connected = false;
var loggedin = false;

var files = {};
var file_curr_index = -1;
var file_curr_name = "";
var file_content = "";
var file_index = -1;
var file_printing = false;
var file_paused   = false;

var cons_count = 0;

var host = (location.hostname=='' ? 'we3p' : location.hostname) + ':8000';

function ebi(x) { return document.getElementById(x); }

function opentab(tab)
{
	if (currtab == tab)
		return;
	console.log('opening tab ' + tab);
	var f = function(name, tab) {
		if (tab == name)
			ebi('nav_'+name).classList.add('active');
		else
			ebi('nav_'+name).classList.remove('active');
	}
	for(var i in tabs) f(tabs[i], tab);
	if (tab=="none") { currtab = tab; return; }
	if (!connected) { opentab("none"); return; }
	
	ebi('cont_' + currtab).style.display = 'none';
	ebi('cont_' + tab).style.display = null;
	
	currtab = tab;
}

function initvals(j)
{
	ebi('conn_shape').value = j.shape;
	ebi('conn_origin').value = j.origin;
	ebi('conn_x').value = j.printarea[0];
	ebi('conn_y').value = j.printarea[1];
	ebi('conn_z').value = j.printarea[2];
	ebi('conn_heatbed').checked = j.heatbed;
	ebi('conn_fx').value = j.feedrate[0];
	ebi('conn_fy').value = j.feedrate[1];
	ebi('conn_fz').value = j.feedrate[2];
	ebi('conn_fe').value = j.feedrate[3];
	ebi('conn_nozzle').value = j.nozzle;
	ebi('conn_baudrate').value = j.baudrate;
}

function file_select(index, name)
{
	if (index == file_curr_index)
		return;
	
	if (file_curr_index >= 0)
		ebi('file' + file_curr_index).style.fontWeight = null;
	file_curr_index = index;
	file_curr_name = name
	console.log("selected file " + name);
	
	ws.send('S' + index);
	ebi('file' + index).style.fontWeight = 'bold';
	
	file_content = "";
	file_index = 0;
	
	ebi('file_start').disabled = null;
}
function file_recv_content(content)
{
	var check = content.charCodeAt(content.length-1);
	var c = content.substr(0, content.length-1);
	var cs = 0;
	for (var i = 0; i < c.length; i++)
		cs ^= c.charCodeAt(i);
	cs &= 0xFF;
	if (cs == check)
	{
		file_content += c;
		file_index += c.length;
		if (file_index < files[file_curr_name].size)
			ws.send('R');
		else
		{
			console.log(new Date());
			file_content = file_content.substr(0, files[file_curr_name].size);
			ebi('file_content').innerHTML = file_content;
			console.log(new Date());
		}
	}
	else
	{
		console.log("checksum invalid");
		ws.send('E');
	}
}
function file_start_print()
{
	ebi('file_start').disabled = true;
	ebi('file_pause').disabled = null;
	ebi('file_stop' ).disabled = null;
	
	console.log("starting to print !!!");
	file_printing = true;
	ws.send('G');
}
function file_pause_print()
{
	if (file_paused)
	{
		ws.send('C');
		ebi('file_pause').className = "";
	}
	else
	{
		ws.send('I');
		ebi('file_pause').className = "down";
	}
	file_paused = !file_paused;
}
function file_stop_print()
{
	ws.send('T');
	file_printing = false;
	file_paused = false;
	
	ebi('file_start').disabled = null;
	ebi('file_pause').disabled = true;
	ebi('file_stop' ).disabled = true;
}

function cons_send()
{
	var inp = ebi('cons_enter');
	if (inp.value.trim() == "")
		return;
	var c = ebi('cons_console');
	ws.send("P" + inp.value);
	var scroll = c.scrollTop == c.scrollHeight - c.clientHeight;
	c.innerHTML += '<div id="cons_ll' + cons_count + '" class="out">' + inp.value + '</div>';
	if (scroll)
		ebi('cons_ll' + cons_count).scrollIntoView();
	cons_count++;
	inp.value = '';
}
function cons_recv(str)
{
	if (file_printing && str.trim() == "ok")
		return;
	
	var c = ebi('cons_console');
	var scroll = c.scrollTop == c.scrollHeight - c.clientHeight;
	c.innerHTML += '<div id="cons_ll' + cons_count + '" class="in">' + str + '</div>';
	if (scroll)
		ebi('cons_ll' + cons_count).scrollIntoView();
	cons_count++;
	
	if (cons_count >= 1000)
	{
		var element = ebi('cons_ll' + (cons_count - 1000));
		if (element != null)
			element.parentNode.removeChild(element);
	}
}

function recv(event)
{
	var d = event.data/*.replace(/[\r\n]/g, '')*/;
	console.log('recv:' + d[0]);
	
	if (d.startsWith("hello"))
	{
		var j = JSON.parse(d.substr(5));
		console.log(j);
		initvals(j);
		connected = true;
		if (currtab == 'none')
			opentab('conn');
		
		ws.send("F");
		
		return;
	}
	
	if (d.startsWith("O"))
	{
		cons_recv(d.substr(1));
		return;
	}
	
	if (d.startsWith("F"))
	{
		var j = JSON.parse(d.substr(1).replace(/[\r\n]/g, ''));
		var keys = Object.keys(j);
		keys.sort();
		console.log(j);
		var html = '';
		for (var i = 0; i < keys.length; i++)
		{
			var f = keys[i];
			if (j[f] == null || j[f] <= 0)
				continue;
			html += '<li><a id="file' + j[f].index + '" onClick="file_select(' + j[f].index + ', \'' + f + '\');">' + f + ' (' + (j[f].size/0x100000).toFixed(3) + ' MiB)<a></li>';
		}
		ebi('files').innerHTML = html;
		files = j;
		return;
	}
	
	if (d.startsWith("R"))
	{
		file_recv_content(atob(d.substr(1)));
		return;
	}
	
	console.log('recv: "' + d + '" - message not processed');
}

function init()
{
	ws = new WebSocket('ws://' + host);
	ws.onopen = function(event)
	{
		console.log("connection established");
		ws.send("hello" + JSON.stringify({
			shape: "rectangular",
			origin: "bottom left",
			printarea: [ 200, 200, 160 ],
			heatbed: true,
			feedrate: [ 6000, 6000, 200, 300 ],
			nozzle: 0.4,
			baudrate: 250000
		}));
	};
	ws.onmessage = recv;
	
	var f = function(name) {
		ebi('nav_'+name).addEventListener('click', function() {
			opentab(name);
		});
		ebi('rnav_'+name).addEventListener('click', function() {
			ebi('rnav').style.display = 'none';
			opentab(name);
		});
	}
	for(var i in tabs) f(tabs[i]);
	
	ebi('navexpand').addEventListener('click', function() {
		ebi('rnav').style.display = 'block';
	});
	ebi('navcollapse').addEventListener('click', function() {
		ebi('rnav').style.display = 'none';
	});
	
	ebi('cons_send').addEventListener('click', function(ev) {
		cons_send();
	});
	ebi('cons_enter').addEventListener('keyup', function(ev) {
		ev.preventDefault();
		if (ev.keyCode == 13)
			cons_send();
	});
}

init();

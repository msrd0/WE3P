#include "server.h"

#include <FS.h>

void serve(String contenttype, String file)
{
    digitalWrite(ledh, 1);
    File f = SPIFFS.open(file, "r");
    server.streamFile(f, contenttype);
    digitalWrite(ledh, 0);
}

void handleNotFound()
{
  digitalWrite(ledh, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(ledh, 0);
}

